from django.urls import path
from .views import DetailView,ListView,UserViewSet,UserDetailViewSet
urlpatterns = [
    path("<int:pk>/",DetailView.as_view()),
    path("",ListView.as_view()),
    path("user/",UserViewSet.as_view()),
    path("user/<int:pk>/",UserDetailViewSet.as_view()),
   

    
]
