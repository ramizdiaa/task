from rest_framework import generics
from articles.models import Article
from .serializers import ArticleSerializer,UserSerializer
from users.models import CustomUser

class ListView(generics.ListCreateAPIView):
    queryset=Article.objects.all()
    serializer_class=ArticleSerializer
class DetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset=Article.objects.all()
    serializer_class=ArticleSerializer
class UserDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class =UserSerializer
class UserViewSet(generics.ListCreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class =UserSerializer   

        

   