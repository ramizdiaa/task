from django.contrib import admin
from .models import Article ,comment
# Register your models here.
class commentInLine(admin.TabularInline):
    model=comment
class ArticleAdmin(admin.ModelAdmin):
    inlines=[
        commentInLine,
    ]    




admin.site.register(Article,ArticleAdmin)
admin.site.register(comment)